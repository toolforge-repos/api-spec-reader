<!-- Reusable RapiDoc OpenAPI spec reader https://github.com/rapi-doc/RapiDoc -->
<!doctype html> <!-- Important: must specify -->
<html>
  <head>
    <title>API spec reader</title>
    <meta name="description" content="Reusable OpenAPI spec reader powered by RapiDoc">
    <meta charset="utf-8"> <!-- Important: rapi-doc uses utf8 characters -->
    <script type="module" src="https://unpkg.com/rapidoc/dist/rapidoc-min.js"></script>
  </head>
  <body>
    <style>
      rapi-doc::part(label-tag-title) {
        text-transform: capitalize;
      }
    </style>
    <rapi-doc
      spec-url="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json"
      theme="dark"
      text-color="#eaeaea"
      regular-font="-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Lato, Helvetica, Arial, sans-serif;"
      render-style="read"
      schema-style="table"
      schema-description-expanded="true"
      default-schema-tab="example"
      fill-request-fields-with-example="false"
      primary-color="#049DFF"
      font-size="large"
      load-fonts="false"
      show-method-in-nav-bar="as-colored-block"
      allow-server-selection="false"
      allow-authentication="false"
      info-description-headings-in-navbar="true"
    >
        <img
            slot="logo"
            src="https://developer.wikimedia.org/assets/wikimedia.svg"
            style="width:30px; height:30px"
        />
    </rapi-doc>
  </body>
</html>

