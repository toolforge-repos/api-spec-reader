# OpenAPI specification reader and sandbox

- [Visit the tool](https://api-spec-reader.toolforge.org/)
- [Read the docs](docs/en/index.md)
- [API spec reader on Toolhub](https://toolhub.wikimedia.org/tools/toolforge-api-spec-reader)

Contributions and translations welcome! Please follow the [Wikimedia code of conduct for technical spaces](https://www.mediawiki.org/wiki/Special:MyLanguage/Code_of_Conduct).
