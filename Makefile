sync:
	echo "Syncing with GitLab"
	git fetch origin
	git reset origin/main --hard
	rm -rf ../public_html
	cp -r public_html ../
