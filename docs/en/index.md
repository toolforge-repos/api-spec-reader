---
title: API spec reader
---

API spec reader is an instance of RapiDoc that you can use to read any local or
publicly available OpenAPI spec. This tool is hosted on Toolforge.

## Development

_Work in progress_

Commit changes to this repository. To update the tool, run this command from the tool directory:

```
cd api-spec-reader && make
```

## About RapiDoc

- [RapiDoc source](https://github.com/rapi-doc/RapiDoc)
- [RapidDoc API reference](https://rapidocweb.com/api.html)

## Test specs

- [Link recommendation API](https://api.wikimedia.org/service/linkrecommendation/apispec_1.json)
