---
title: Decision records
---

This page documents decisions made about the API spec reader project.

## RapiDoc render style

### Status
Accepted

### Context
[RapiDoc](https://www.mediawiki.org/wiki/Documentation/API_documentation#RapiDoc_(UI)) is a more lightweight alternative to [swagger-ui](https://github.com/swagger-api/swagger-ui) ([example](https://wikimedia.org/api/rest_v1/#/)), a popular tool for viewing OpenAPI specifications. This decision chooses a layout for the API spec reader tool from the options offered by RapiDoc.

### Decision
RapiDoc supports three layouts (called `render-styles` in RapiDoc documentation): view, read, and focused<sup>[1]</sup>.

- view layout ([example](https://toolhub.wikimedia.org/api-docs)): organized into expanding and collapsing sections; no sidebar; similar to swagger-ui
- read layout ([example](https://rapidocweb.com/examples/read-mode.html)): organized into a continuous page with a sidebar for navigation
- focused layout ([example](https://rapidocweb.com/examples/focused-mode.html)): organized into separate pages with a sidebar for navigation

Of the three layouts, the read layout is the easiest to navigate. The read layout allows users to navigate in three ways: the sidebar, scrolling, and <kbd>control</kbd>+<kbd>f</kbd>. In comparison, the focused layout only allows navigation through the sidebar, which may not be obvious to an audience used to the single-column layout of swagger-ui. This issue could be fixed by including a list of endpoints in the overview section, but adding a list of endpoints into the API specification creates too much duplication. In the view layout, the expandable sections are cumbersome to expand and collapse, resulting in a long page that's easy to get lost on. Neither the view layout nor the focused layout supports navigation through <kbd>control</kbd>+<kbd>f</kbd>.

However, the read layout has a drawback: the URL automatically updates with the anchor of the currently viewed section, making link sharing messy and sometimes loading the doc in the middle of the page. This functionality is solved by the focused layout, but the drawbacks of the focused layout outweigh this benefit.

Another consideration is mobile responsiveness. Looking at comparable pages, mobile traffic makes up 6% of visits to the MediaWiki REST API docs<sup>[2]</sup> and 7% of visits to the OOUI demos site<sup>[3]</sup>. Although mobile devices are less common with users of API docs and other technical docs, supporting mobile makes the tool more inclusive. However, RapiDoc officially supports a minimum width of 768px<sup>[4]</sup>. 

- view layout: mostly usable on mobile, although some elements, such as allowed values, are cut off
- read layout: nearly the same on desktop and mobile, making most of the text extremely small on mobile, similar to the mobile experience of swagger-ui
- focused layout: unusable on mobile since the sidebar is not visible

Considering these factors, I decided to choose the read layout for the API spec reader.

### Consequences

The API spec reader will be easy to navigate but have a poor mobile experience, and users may be navigated to the middle of the page accidentally. The two existing Wikimedia uses of RapiDoc (Toolhub and the spec reader) will use different layouts, and users familiar with swagger-ui may be confused.

[1]: https://rapidocweb.com/api.html#att-layout
[2]: https://pageviews.wmcloud.org/?project=mediawiki.org&platform=desktop&agent=user&redirects=0&range=this-year&pages=API:REST_API/Reference
[3]: https://piwik.wikimedia.org/index.php?action=index&date=today&idSite=21&module=CoreHome&period=day&updated=2#?idSite=21&period=year&date=2023-04-01&category=General_Visitors&subcategory=DevicesDetection_Devices
[4]: https://github.com/rapi-doc/RapiDoc/issues/453#issuecomment-807569847  
